﻿using System.Web.Mvc;
using System.Web.Routing;
using MedInsider.WebUI.Infrastructure;

namespace MedInsider.WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            AppIdentityDbContext db = new AppIdentityDbContext();
            db.Database.Initialize(true);
            
        }
    }
}
