﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using MedInsider.Domain.Abstract;
using MedInsider.Domain.Entities;

namespace MedInsider.WebUI.Infrastructure
{
    public class ArticleRepository : IRepository<Article>
    {
        private AppIdentityDbContext context = HttpContext.Current.GetOwinContext().Get<AppIdentityDbContext>();

        public ICollection<Article> Items
        {
            get { return context.Articles.ToList(); }
        }

        public void Insert(Article entity)
        {
            context.Articles.Add(entity);
        }

        public void Delete(object id)
        {
            var entityToDelete = context.Articles.Find(id);
            Delete(entityToDelete);
        }

        public void Delete(Article entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                context.Articles.Attach(entityToDelete);
            }
            context.Articles.Remove(entityToDelete);
        }

        public virtual void Update(Article entityToUpdate)
        {
            context.Articles.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public Article GetById(object id)
        {
            return context.Articles.Find(id);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}
