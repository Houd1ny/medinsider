﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MedInsider.Domain.Entities;

namespace MedInsider.WebUI.Infrastructure
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        
        public AppIdentityDbContext()
            : base("MedInsider-db")
        {
        }

        static AppIdentityDbContext()
        {
            Database.SetInitializer<AppIdentityDbContext>(new IdentityDbInit());
        }

        public static AppIdentityDbContext Create()
        {
            return new AppIdentityDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Comment>()
               .HasRequired(c => c.Article)
               .WithMany(t => t.Comments)
               .WillCascadeOnDelete();

            modelBuilder.Entity<Comment>()
                .HasRequired(c => c.Author)
                .WithMany(u => u.Comments)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Article>()
                .HasRequired(t => t.Author)
                .WithMany(u => u.Articles)
                .WillCascadeOnDelete(false);

        }
    }

    public class IdentityDbInit : CreateDatabaseIfNotExists<AppIdentityDbContext>
    {
        protected override void Seed(AppIdentityDbContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }

        public void PerformInitialSetup(AppIdentityDbContext context)
        {
            AppUserManager userMgr = new AppUserManager(new UserStore<AppUser>(context));
            AppRoleManager roleMgr = new AppRoleManager(new RoleStore<AppRole>(context));
            string roleName = "Admin";
            string userName = "Houd1ny";
            string password = "Qqwerty123";
            string email = "priymayuriy@gmail.com";
            if (!roleMgr.RoleExists(roleName))
            {
                roleMgr.Create(new AppRole(roleName));
            }
            AppUser user = userMgr.FindByName(userName);
            if (user == null)
            {
                userMgr.Create(new AppUser
                {
                    UserName = userName, Email = email,
                    FirstName = "Yuriy", LastName = "Pryyma"
                },
                password);
                user = userMgr.FindByName(userName);
            }
            if (!userMgr.IsInRole(user.Id, roleName))
            {
                userMgr.AddToRole(user.Id, roleName);
            }

            string userRole = "User";
            if (!roleMgr.RoleExists(userRole))
            {
                roleMgr.Create(new AppRole(userRole));
            }
            userRole = "Doctor";
            if (!roleMgr.RoleExists(userRole))
            {
                roleMgr.Create(new AppRole(userRole));
            }
        }
    }
}