﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using MedInsider.Domain.Abstract;
using MedInsider.Domain.Entities;

namespace MedInsider.WebUI.Infrastructure
{
    public class CommentRepository : IRepository<Comment>
    {
        private AppIdentityDbContext context = HttpContext.Current.GetOwinContext().Get<AppIdentityDbContext>();

        public ICollection<Comment> Items
        {
            get { return context.Comments.ToList(); }
        }

        public void Insert(Comment entity)
        {
            context.Comments.Add(entity);
        }

        public void Delete(object id)
        {
            var entityToDelete = context.Comments.Find(id);
            Delete(entityToDelete);
        }

        public void Delete(Comment entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                context.Comments.Attach(entityToDelete);
            }
            context.Comments.Remove(entityToDelete);
        }

        public virtual void Update(Comment entityToUpdate)
        {
            context.Comments.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public Comment GetById(object id)
        {
            return context.Comments.Find(id);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}
