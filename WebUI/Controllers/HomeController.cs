﻿using System.Web.Mvc;

namespace MedInsider.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}