﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MedInsider.Domain.Abstract;
using MedInsider.Domain.Entities;
using MedInsider.WebUI.Infrastructure;
using MedInsider.WebUI.Models;
using System;


namespace WebUI.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    
    {
        public AdminController(IRepository<Article> articleRepository,
            IRepository<Comment> commentRepository)
        {
            this.articleRepository = articleRepository;
            this.commentRepository = commentRepository;
        }

        private IRepository<Article> articleRepository;

        private IRepository<Comment> commentRepository;

        [Authorize(Roles = "Admin")]
        public ActionResult Users()
        {
            return View(UserManager.Users);
        }
        public async Task<ActionResult> EditUser(string email)
        {
            if (email != null)
            {
                AppUser user = await UserManager.FindByEmailAsync(email);
                if (user != null)
                {
                    ViewBag.AllRoles = RoleManager.Roles;
                    ViewBag.UserRoles = UserManager.GetRoles(user.Id);
                    return View(user);
                }
                
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<ActionResult> UpdateRoles(FormCollection collection)
        {
            string userEmail = collection["userEmail"];
            string collectionRoles = collection["SelectedGroups"];
            AppUser user = UserManager.FindByEmail(userEmail);
            var userRoles = UserManager.GetRoles(user.Id).ToArray();
            var result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles);
            if (collectionRoles != null)
            { 
                var selectedRoles = collectionRoles.Split(',');
                result = await UserManager.AddToRolesAsync(user.Id, selectedRoles); 
            }
            return RedirectToAction("Users", "Admin");
        }   

        public ActionResult UserComments(string email)
        {
            if (email == null)
            {
                email = "";
            }
            var user = UserManager.FindByEmail(email);
            if (user == null)
            {
                return RedirectToAction("Users", "Admin");
            }
            return View(user.Comments);
        }

        [HttpPost]
        public ActionResult UserComments(FormCollection collection)
        {
            var collectionRoles = collection["SelectedGroups"].Split(',');
            var userEnail = collection["userEmail"];
            var user = UserManager.FindByEmail(userEnail);
            if (user == null)
            {
                return RedirectToAction("Users", "Admin");
            }
            foreach (var comment in user.Comments)
            {
                if (collectionRoles.Contains(Convert.ToString(comment.CommentId)))
                { 
                    comment.IsBlocked = true;     
                }
                else
                {
                    comment.IsBlocked = false;  
                }
                commentRepository.Update(comment);
                commentRepository.SaveChanges();
            }
            return View(user.Comments);
        }
        

        protected AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        protected AppRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppRoleManager>();
            }
        }
        public IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

	}
}