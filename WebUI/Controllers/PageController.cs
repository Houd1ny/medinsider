﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MedInsider.Domain.Abstract;
using MedInsider.Domain.Entities;
using MedInsider.WebUI.Infrastructure;
using MedInsider.WebUI.Models;

namespace MedInsider.WebUI.Controllers
{
    [Authorize]
    public class PageController : Controller
    {
        public PageController(IRepository<Article> articleRepository,
            IRepository<Comment> commentRepository)
        {
            this.articleRepository = articleRepository;
            this.commentRepository = commentRepository;
        }

        public ActionResult Index()
        {
            return View(articleRepository.Items);
        }

        public ActionResult CreateArticle()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> CreateArticle(FormCollection collection)
        {
            string articleText = collection["noise"];
            string title = collection["title"];
            string userId = collection["userId"];
            var user = await UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                return HttpNotFound("User not found.");
            }

            var article = new Article
            {
                Author = user,
                AuthorId = user.Id,
                Text = articleText,
                Date = DateTime.UtcNow,
                Title = title,
            };

            articleRepository.Insert(article);
            articleRepository.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Search(string Search)
        {
            if (Search==null)
            {
                Search = "";
            }
            var articles = (from article in articleRepository.Items
                            where article.Title.Contains(Search)
                            orderby article.Date descending
                            select article).Take(5);
            return View(articles);
        }

        [AllowAnonymous]
        public ActionResult ViewArticle(int? articleId)
        {
            if (articleId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var article = articleRepository.GetById(articleId);
            if (article == null)
            {
                return HttpNotFound();
            }
            var user = UserManager.FindById(User.Identity.GetUserId());
            ViewBag.Comments = article.Comments;
            return View(article);
        }

        [HttpPost]
        public ActionResult AddComment(FormCollection collection)
        {
            string comment = collection["comment"];
            int articleId = Int32.Parse(collection["ArticleId"]);
            var user = UserManager.FindById(User.Identity.GetUserId());
            var article = articleRepository.GetById(articleId);

            var newComment = new Comment
            {
                AuthorId = user.Id,
                Author = user,
                Content = comment,
                Date = DateTime.UtcNow,
                Article = article,
                ArticleId = articleId
            };
            commentRepository.Insert(newComment);
            commentRepository.SaveChanges();

            return RedirectToAction("ViewArticle", new { articleId = articleId });
        }
        

        public ActionResult ViewArticle1(int? article_id)
        {
            if (article_id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var article = articleRepository.GetById(article_id);
            if (article == null)
            {
                return HttpNotFound();
            }
            var user = UserManager.FindById(User.Identity.GetUserId());
            var viewModel = new ArticleDetailsViewModel
            {
                Article = article,
                NewComment = new Comment
                {
                    Content = null,
                    AuthorId = user.Id,
                    ArticleId = article.ArticleId,
                }
            };

            return View(viewModel);
        }

        public ActionResult CommentOnArticle(Comment newComment)
        {
            return PartialView(newComment);
        }

        public ActionResult ViewComment(Comment comment)
        {
            return PartialView(comment);
        }


        private IRepository<Article> articleRepository;

        private IRepository<Comment> commentRepository;
        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        } 

       
    }
}