﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MedInsider.Domain.Entities;

namespace MedInsider.WebUI.Models
{
    public class CategoryCreationViewModel
    {
        [Required(ErrorMessage = "Please name a category")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please provide short description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

    }
  
    public class CategoryEditViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public IEnumerable<AppUser> Experts { get; set; }

        public IEnumerable<AppUser> NonExperts { get; set; }
    }

    public class CategoryModificationViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please name a category")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please provide short description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string[] IdsToAdd { get; set; }

        public string[] IdsToDelete { get; set; }
    }
}