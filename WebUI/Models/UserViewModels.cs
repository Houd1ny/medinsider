﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MedInsider.Domain.Entities;

namespace MedInsider.WebUI.Models
{
    public class VerdictViewModel
    {
        public int RequestId { get; set; }

        public bool IsApproved { get; set; }

        [Display(Name = "Denial Reason")]
        public string DenialMessage { get; set; }

        [Required]
        public int AdminId { get; set; }

        //public List<string> AddedFiles { get; set; }

        public virtual AppUser Admin { get; set; }

        //public virtual ICollection<CertificationFile> Files { get; set; }
    }

    public class ApplyViewModel
    {
        [Required]
        [Display(Name = "Request Message")]
        public string ApplicationText { get; set; }

        [Required]
        public int CategoryId { get; set; }

    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Login Name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }
        
        [Required]
        public string Email { get; set; }
        
        [Required]
        public string Password { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Login Name")]
        public string Name { get; set; }

        [Required]
        public string Password { get; set; }
    }
}