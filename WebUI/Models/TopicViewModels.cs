﻿using System.ComponentModel.DataAnnotations;
using MedInsider.Domain.Entities;

namespace MedInsider.WebUI.Models
{
    public class ArticleDetailsViewModel
    {
        [Required]
        public Article Article { get; set; }

        [Required]
        public Comment NewComment { get; set; }
    }

    public class ArticleCreationViewModel
    {
        [Required(ErrorMessage = "Please enter a short title for your problem")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please specify category of your problem")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Please provide detailed information on your problem")]
        [DataType(DataType.MultilineText)]
        public string InitialCommentContent { get; set; }
    }
}