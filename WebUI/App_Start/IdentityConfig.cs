﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using Microsoft.Owin.Security;
using MedInsider.WebUI.Infrastructure;
using Microsoft.AspNet.Identity.Owin;
using MedInsider.Domain.Entities;
using System.Security.Claims;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MedInsider.Domain.Abstract;
using MedInsider.WebUI.Models;


namespace MedInsider.WebUI
{
    public class IdentityConfig
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<AppIdentityDbContext>(AppIdentityDbContext.Create);
            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.CreatePerOwinContext<AppRoleManager>(AppRoleManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
    {
        // this function is executed every http request and executed very early in the pipeline
        // and here you have access to cookie properties and other low-level stuff. 
        // makes sense to have the invalidation here
        OnValidateIdentity = async context =>
        {
            // invalidate user cookie if user's security stamp have changed
            var invalidateBySecirityStamp = SecurityStampValidator.OnValidateIdentity<AppUserManager, AppUser>(
                    validateInterval: TimeSpan.FromSeconds(12),
                    regenerateIdentity: (manager, user) => manager.CreateIdentityAsync(user,
                        DefaultAuthenticationTypes.ApplicationCookie));
            await invalidateBySecirityStamp.Invoke(context);

            if (context.Identity == null || !context.Identity.IsAuthenticated)
            {
                return;
            }
            // get user manager. It must be registered with OWIN
            var userManager = context.OwinContext.GetUserManager<AppUserManager>();
            var username = context.Identity.Name;

            // get new user identity with updated properties
            var updatedUser = await userManager.FindByNameAsync(username);

            var roleManager = context.OwinContext.GetUserManager<AppRoleManager>();
            var userRoles = userManager.GetRoles(updatedUser.Id).ToArray();

            bool flag = false;

            //foreach(var role in roleManager.Roles){
            //    bool a1 = userRoles.Contains(role.Name);
            //    bool a2 = context.OwinContext.Request.User.IsInRole(role.Name);
            //    if (!(a1 ^ a2))
            //    {
            //        flag = true;
            //    }

            //}
            flag = false;
            if (flag)
            {

                // updated identity from the new data in the user object

                var newIdentity = userManager.CreateIdentityAsync(updatedUser,
                        DefaultAuthenticationTypes.ApplicationCookie);

                // kill old cookie

                context.OwinContext.Authentication.SignOut(context.Options.AuthenticationType);

                ClaimsIdentity[] claims = new ClaimsIdentity[1];
                claims[0] = await newIdentity;

                // sign in again
                var authenticationProperties = new AuthenticationProperties() { IsPersistent = context.Properties.IsPersistent };
                context.OwinContext.Authentication.SignIn(authenticationProperties, claims);
            }
        }
    }
            });
        }


    }
}