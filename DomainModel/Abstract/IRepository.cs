﻿using System.Collections.Generic;

namespace MedInsider.Domain.Abstract
{
    public interface IRepository<T> where T: class
    {
        ICollection<T> Items { get; }

        void Insert(T entity);
        
        void Delete(object id);
        
        void Delete(T entityToDelete);
        
        void Update(T entity);
        
        T GetById(object id);

        void SaveChanges();
    }
}
