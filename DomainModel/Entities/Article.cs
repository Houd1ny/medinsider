﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MedInsider.Domain.Entities
{
    public class Article
    {
        [Key]
        public int ArticleId { get; set; }
        
        [Required]
        public string AuthorId { get; set; }

        public string Text { get; set; }
        
        [Required]
        public string Title { get; set; }
        
        [Required]
        public DateTime Date { get; set; }

        public bool IsBlocked { get; set; }

        public virtual AppUser Author { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
