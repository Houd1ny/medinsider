﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace MedInsider.Domain.Entities
{
    public class AppRole : IdentityRole
    {
        public AppRole()
        {           
        }

        public AppRole(string name)
            : base(name)
        {
        }
    }
}