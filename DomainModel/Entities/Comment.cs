﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MedInsider.Domain.Entities
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }

        [Required]
        public string AuthorId { get; set; }
        
        [Required]
        public int ArticleId { get; set; }

        [Required(ErrorMessage = "Please fill your comment with content")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        public bool IsBlocked { get; set; }

        [Required]
        public DateTime Date { get; set; }
        
        public virtual AppUser Author { get; set; }
        public virtual Article Article { get; set; }
    }
}
