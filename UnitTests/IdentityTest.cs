﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MedInsider.Domain.Abstract;
using MedInsider.Domain.Entities;
using MedInsider.WebUI.Controllers;
using MedInsider.WebUI.Models;

namespace MedInsider.UnitTests
{
    [TestClass]
    public class IdentityTest
    {
        [TestMethod]
        public void Can_Create_User()
        {
            //Arrange
            var user = new AppUser { UserName = "JohnDoe", Email = "johndoe@example.com" };
            var mockStore = new Mock<IUserStore<AppUser>>();

            var userManager = new UserManager<AppUser>(mockStore.Object);
            mockStore.Setup(x => x.CreateAsync(user))
                        .Returns(Task.FromResult(IdentityResult.Success));

            mockStore.Setup(x => x.FindByNameAsync(user.UserName))
                        .Returns(Task.FromResult(user));


            //Act
            Task<IdentityResult> tt = (Task<IdentityResult>)mockStore.Object.CreateAsync(user);
            var actualUser = userManager.FindByName("JohnDoe");

            //Assert
            Assert.AreEqual("JohnDoe", actualUser.UserName);
            Assert.AreEqual("johndoe@example.com", actualUser.Email);
        }

        [TestMethod]
        public void Can_Store_User_Additional_Properties()
        {
            //Arrange
            var user = new AppUser
            {
                UserName = "JohnDoe", 
                Email = "johndoe@example.com",
                FirstName = "John",
                LastName = "Doe"
            };
            var mockStore = new Mock<IUserStore<AppUser>>();

            var userManager = new UserManager<AppUser>(mockStore.Object);
            mockStore.Setup(x => x.CreateAsync(user))
                        .Returns(Task.FromResult(IdentityResult.Success));

            mockStore.Setup(x => x.FindByNameAsync(user.UserName))
                        .Returns(Task.FromResult(user));


            //Act
            Task<IdentityResult> tt = (Task<IdentityResult>)mockStore.Object.CreateAsync(user);
            var actualUser = userManager.FindByName("JohnDoe");

            //Assert
            Assert.AreEqual("John", actualUser.FirstName);
            Assert.AreEqual("Doe", actualUser.LastName);
        }

        [TestMethod]
        public void Can_Create_Role()
        {
            var role = new AppRole { Name = "Admin" };
            var roleStore = new Mock<IRoleStore<AppRole>>();

            var roleManager = new RoleManager<AppRole, string>(roleStore.Object);
            roleStore.Setup(x => x.CreateAsync(role))
                .Returns(Task.FromResult(IdentityResult.Success));

            roleStore.Setup(x => x.FindByNameAsync(role.Name))
                .Returns(Task.FromResult(role));

            //Act
            
            Task<IdentityResult> tt = (Task<IdentityResult>) roleStore.Object.CreateAsync(role);
            var actualRole = roleManager.FindByName("Admin");

            //Assert
            Assert.AreEqual("Admin", actualRole.Name);
        }

        [TestMethod]
        public void Can_Update_Role()
        {
            var role = new AppRole { Name = "Admin" };
            var roleStore = new Mock<IRoleStore<AppRole>>();

            var roleManager = new RoleManager<AppRole, string>(roleStore.Object);
            roleStore.Setup(x => x.CreateAsync(role))
                .Returns(Task.FromResult(IdentityResult.Success));

            roleStore.Setup(x => x.UpdateAsync(role))
                .Returns(Task.Run( () =>
                {
                    role.Name = "User";
                    return IdentityResult.Success; 
                }));

            roleStore.Setup(x => x.FindByNameAsync(role.Name))
                .Returns(Task.FromResult(role));

            //Act

            Task<IdentityResult> tt = (Task<IdentityResult>)roleStore.Object.CreateAsync(role);
            Task<IdentityResult> tt1 = (Task<IdentityResult>)roleStore.Object.UpdateAsync(role);
            var actualRole = roleManager.FindByName("User");

            //Assert
            Assert.AreEqual("User", actualRole.Name);
        }
        
    }
}
