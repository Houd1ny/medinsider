﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using Microsoft.Owin.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MedInsider.Domain.Abstract;
using MedInsider.Domain.Entities;
using MedInsider.WebUI.Controllers;

namespace MedInsider.UnitTests
{
    [TestClass]
    public class AccountControllerTest
    {
        [TestMethod]
        public void Can_Call_Login_Method()
        {

            var identity = new Mock<IIdentity>();
            identity.SetupGet(x => x.IsAuthenticated).Returns(true);

            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(identity.Object);

            var httpContext = new Mock<HttpContextBase>();
            httpContext.SetupGet(x => x.User).Returns(principal.Object);


            var controller = new AccountController();
            controller.ControllerContext =
                new ControllerContext(new RequestContext(httpContext.Object, new RouteData()),
                    controller);
            var actionResult = controller.Login("/");
        }

        [TestMethod]
        public void Login_Method_Fail_For_Authorized()
        {

            var identity = new Mock<IIdentity>();
            identity.SetupGet(x => x.IsAuthenticated).Returns(true);

            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(identity.Object);

            var httpContext = new Mock<HttpContextBase>();
            httpContext.SetupGet(x => x.User).Returns(principal.Object);


            var controller = new AccountController();
            controller.ControllerContext =
                new ControllerContext(new RequestContext(httpContext.Object, new RouteData()),
                    controller);
            var actionResult = (ViewResult) controller.Login("/");
            Assert.AreEqual("Access Denied", ((IEnumerable<string>)actionResult.Model).ElementAt(0)); 
        }

        [TestMethod]
        public void Can_Call_Register_Method()
        {
            var identity = new Mock<IIdentity>();
            identity.SetupGet(x => x.IsAuthenticated).Returns(true);

            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(identity.Object);

            var httpContext = new Mock<HttpContextBase>();
            httpContext.SetupGet(x => x.User).Returns(principal.Object);


            var controller = new AccountController();
            var actionResult = controller.Register();
        }

        [TestMethod]
        public void Can_Call_Logout_Method()
        {
            var identity = new Mock<IIdentity>();
            identity.SetupGet(x => x.IsAuthenticated).Returns(true);

            var principal = new Mock<IPrincipal>();
            principal.SetupGet(x => x.Identity).Returns(identity.Object);

            var httpContext = new Mock<HttpContextBase>();
            httpContext.SetupGet(x => x.User).Returns(principal.Object);


            var controller = new AccountController();
            
            controller.ControllerContext =
                new ControllerContext(new RequestContext(httpContext.Object, new RouteData()),
                    controller);
            //var actionResult = controller.Logout();
        }

    }
}
