using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using DomainModel.Entities;
using WebUI.Infrastructure;
using System.Data.Entity.Migrations;

namespace WebUI.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AppIdentityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "WebUI.Infrastructure.AppIdentityDbContext";
        }

        protected override void Seed(AppIdentityDbContext context)
        {
            var userMgr = new AppUserManager(new UserStore<AppUser>(context));
            var roleMgr = new AppRoleManager(new RoleStore<AppRole>(context));
            var roleName = "Admin";
            var userName = "admin";
            var password = "Admin123";
            var email = "admin@example.com";
            if (!roleMgr.RoleExists(roleName))
            {
                roleMgr.Create(new AppRole(roleName));
            }
            var user = userMgr.FindByName(userName);
            if (user == null)
            {
                userMgr.Create(new AppUser
                {
                    UserName = userName, 
                    Email = email, 
                    FirstName = "Admin",
                    LastName = "Adminov",
                    DateOfBirth = new DateTime(1996, 08, 02),
                },
                password);
                user = userMgr.FindByName(userName);
            }
            if (!userMgr.IsInRole(user.Id, roleName))
            {
                userMgr.AddToRole(user.Id, roleName);
            }

            context.SaveChanges();
        }
    }
}
